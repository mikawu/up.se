from ntpath import basename, split
from sys import argv
from sys import exit
from time import sleep
from subprocess import check_call
from json import loads, dumps
import re


__module_name__ = "Pomf.se/clone uploader"
__module_author__ = "Mika Wu"
__module_version__ = "0.2.0.150914"
__module_description__ = "Command line interface for Pomf.se/clone file upload."


# 2015-06-09 Neku announced shutting down pomf this month
# So gotta use one of the clones by default.
# 2015-07-02 ALL THE CLONES ARE DEAD
# 2015-07-20 1339.cf is up now, and there are several new
# "pomf-like"s being developed. Some defaults are below.
# Add more in the same format along with their file form format.
# "shortname" : ["url to upload api", "file form format", "response format", (pass data?), "data format"]

upload_format = {
    "jii" : ["https://jii.moe/api/v1/upload", "file", "https://jii.moe/"],
    "1339" : ["http://1339.cf/upload.php", "files[]", "http://a.1339.cf/"],
    "maxfile" : ["https://maxfile.ro/upload.php", "files[]", "https://d.maxfile.ro/"],
    "uguu" : ["https://www.uguu.se/api.php?d=upload-tool", "file", "https://a.uguu.se/", {'name': '', 'randomname': ''}],
    "catbox" : ["http://catbox.moe/user/api.php", "fileToUpload", "http://files.catbox.moe/", {'reqtype': 'fileupload'}]
}


try:
    import requests
except ImportError:
    print("Module requires Requests library.")
    print("Install with 'pip install requests'.")
    exit()


def usage():
    """Referred to user with invalid input."""
    print("Usage: " + argv[0] + " <service> <file>")
    print("Available services: " + (" ".join(k for k,r in upload_format.items())))
    print("Add more by editing this file.")
    exit()


def fpath(path, return_path=False):
    """Failsafe file-name return. Works across various OSes...probably."""
    head, tail = split(path)
    if return_path:
        return head
    return tail or basename(head)


def clip(txt):
    """Copies upload location to user clipboard automatically."""
    cmd = 'echo '+ txt.strip() + '|clip'
    return check_call(cmd, shell=True)


def extract(node, json_block):
    """Extract given value "node" from json passed and return a
    string containing the 'url ending'
    """
    # print(json_block)
    returls = []

    def _snatch(_dict):
        try:
            returls.append(_dict[node])
        except KeyError:
            pass
        return _dict

    try:
        loads(json_block, object_hook = _snatch)
    except ValueError:
        returls.append(json_block)
    return returls


def main():
    """Attempts to upload argument file to uguu.se servers."""
    if len(argv) < 3:
        usage()

    url = argv[1]
    upload = argv[2]
    dbg = (len(argv) > 3 and argv[3] == 'dbg')

    files = {upload_format[url][1]: open(upload, "rb")}
    data = upload_format[url][3] if len(upload_format[url]) > 3 else None
    #files.update(upload_format[url][4]) if upload_format[url][3] else None

    if dbg:
        print(files)

    try:
        response = requests.post(url = upload_format[url][0], files = files, data = data)
    except Exception as e:
        print("Error uploading: {0}".format(e))
        input()
        exit()

    if dbg:
        print(response.request.headers)

    local = fpath(upload)

    remote = extract("url", response.text)
    print(remote)
    allurl = ""
    for f in remote:
        fullurl = upload_format[url][2] + \
        re.search("(?:(?:(?:.*\://)?(?:www.)?(?:[^\/]+))/)?(.*)", f).group(1) + "\n"
        allurl += fullurl
        print(local + " -> " + fullurl) 
    clip(fullurl)
    print("\a")
    sleep(2)


if __name__ == '__main__':
    main()
