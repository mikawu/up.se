# up.se
Command-line uploading tools for various file hosts

### To add an uploader to your right-click context menu on Windows (useful with these scripts):

1. Create new key in HKEY_CLASSES_ROOT\*\shell\ named however you want the context-item to appear.
2. Create a subkey of the custom key called "command" (ex. HKEY_CLASSES_ROOT\*\shell\pomf\command)
3. Edit default key in "command" to have (in quotes):
  1. Location of Python installation
  2. Location of script
  3. %1 (if you do not include quotes around the parameter you will not be able to upload files with spaces in the name)
4. Default key contents may look something like:  
`"C:\Python27\python.exe" "C:\scripts\pomf.py" "uguu" "%1"`
5. Close regedit and make sure it works

### For a version that is "invisible" with notification tone (no cmd window on Windows):
1. Make a .vbs file with these contents, replacing paths as appropriate

    ```vbs
    Set oShell = CreateObject ("Wscript.Shell")
    Dim strArgs
    strArgs = "cmd /c py C:\scripts\pomf.py " & WScript.Arguments(0) & " """ & WScript.Arguments(1) & ""
    oShell.Run strArgs, 0, True
    ```
        
2. Change your registry key to:  
`Wscript.exe "C:\scripts\pomf.vbs" "uguu" "%1"`  
![alt text](context.png "Note key location.")

**Current only supports Uguu.se and Pomf.se / Pomf.se clones.**